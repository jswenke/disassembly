# Disassembly
This reaDme contains snippets of disassembly for the binary file `unk.bin`. The binary is a flash memory dump of a Renesas SuperH processor.The disassembly includes the function, a snippet of a calling function, and some data that is referenced before the calling function. We would like to better understand the purpose of the Function located at `0x29740` labled `Uknown_Function`.
## Deliverables
At a minimum, you should:
* Privately fork this repository and commit your work there
* Document the input(s) and output(s) of Unknown_Function
* Document what, if anything, the input(s) are validated against
* Explain the purpose of Unknown_Function

For the overachievers:
* Every line should be documented
* All logical blocks should be explained
* Write pseudo-code for Unknown_Function
## The Code
### Function to Decode
```
ROM:00029740       Unknown_Function:                       ; CODE XREF: FuncGen_00440+36↓p
ROM:00029740                                               ; sub_3D5C4+60↓p ...
ROM:00029740 90 25                 mov.w   #h'F0, r0
ROM:00029742 65 5C                 extu.b  r5, r5
ROM:00029744 35 02                 cmp/hs  r0, r5
ROM:00029746 89 20                 bt      locret_2978A
ROM:00029748 63 53                 mov     r5, r3
ROM:0002974A 45 09                 shlr2   r5
ROM:0002974C 45 09                 shlr2   r5
ROM:0002974E 61 07                 not     r0, r1
ROM:00029750 23 19                 and     r1, r3
ROM:00029752 43 18                 shll8   r3
ROM:00029754 43 08                 shll2   r3
ROM:00029756 43 08                 shll2   r3
ROM:00029758 34 5C                 add     r5, r4
ROM:0002975A 61 40                 mov.b   @(0,r4), r1
ROM:0002975C 84 41                 mov.b   @(1,r4), r0
ROM:0002975E 61 1C                 extu.b  r1, r1
ROM:00029760 60 0C                 extu.b  r0, r0
ROM:00029762 30 12                 cmp/hs  r1, r0
ROM:00029764 89 09                 bt      loc_2977A
ROM:00029766 67 13                 mov     r1, r7
ROM:00029768 31 08                 sub     r0, r1
ROM:0002976A 06 1A                 sts     macl, r6
ROM:0002976C 01 37                 mul.l   r3, r1
ROM:0002976E 01 1A                 sts     macl, r1
ROM:00029770 46 1A                 lds     r6, macl
ROM:00029772 60 73                 mov     r7, r0
ROM:00029774 41 29                 shlr16  r1
ROM:00029776 00 0B                 rts
ROM:00029778 30 18                 sub     r1, r0
ROM:0002977A       ; ---------------------------------------------------------------------------
ROM:0002977A
ROM:0002977A       loc_2977A:                              ; CODE XREF: Unknown_Function+24↑j
ROM:0002977A 30 18                 sub     r1, r0
ROM:0002977C 06 1A                 sts     macl, r6
ROM:0002977E 00 37                 mul.l   r3, r0
ROM:00029780 00 1A                 sts     macl, r0
ROM:00029782 46 1A                 lds     r6, macl
ROM:00029784 40 29                 shlr16  r0
ROM:00029786 00 0B                 rts
ROM:00029788 30 1C                 add     r1, r0
ROM:0002978A       ; ---------------------------------------------------------------------------
ROM:0002978A
ROM:0002978A       locret_2978A:                           ; CODE XREF: Unknown_Function+6↑j
ROM:0002978A 00 0B                 rts
ROM:0002978C 84 4F                 mov.b   @(h'F,r4), r0
ROM:0002978C       ; End of function Unknown_Function
ROM:0002978C
ROM:0002978C       ; ---------------------------------------------------------------------------
ROM:0002978E 00 F0 word_2978E:     .data.w h'F0            ; DATA XREF: Unknown_Function↑r
```

### Caller of Uknown Function
```
ROM:0004B638 C0 00                 mov.b   r0, @((byte_FFFE90A1 - h'FFFE90A1),gbr)
ROM:0004B63A 85 F2                 mov.w   @(h'3C+var_38,r15), r0
ROM:0004B63C 42 21                 shar    r2
ROM:0004B63E D1 36                 mov.l   #byte_FFFE933A, r1
ROM:0004B640 72 7F                 add     #h'7F, r2
ROM:0004B642 D4 36                 mov.l   #unk_FFFE87D8, r4
ROM:0004B644 81 B2                 mov.w   r0, @((word_FFFE8594 - h'FFFE8590),r11)
ROM:0004B646 72 01                 add     #1, r2
ROM:0004B648 84 12                 mov.b   @((byte_FFFE933C - h'FFFE933A),r1), r0 ;
ROM:0004B64A 24 21                 mov.w   r2, @(0,r4) ; unk_FFFE87D8
ROM:0004B64C D4 34                 mov.l   #unk_Data, r4
ROM:0004B64E DA 35                 mov.l   #Unknown_Function, r10 ;
ROM:0004B64E                                               ;
ROM:0004B650 28 C0                 mov.b   r12, @(0,r8) ; byte_FFFE8FE2
ROM:0004B652 4A 0B                 jsr     @r10            ;
ROM:0004B652                                               ;
ROM:0004B654 65 0C                 extu.b  r0, r5
ROM:0004B656 6E 80                 mov.b   @(0,r8), r14 ; byte_FFFE8FE2
ROM:0004B658 60 0C                 extu.b  r0, r0
ROM:0004B65A 6C EC                 extu.b  r14, r12
ROM:0004B65C 0C 07                 mul.l   r0, r12
```

### Data Reference
```
ROM:000105C3 64    unk_Data:       .data.b h'64 ; d        ; DATA XREF: 
ROM:000105C4 64                    .data.b h'64 ; d
ROM:000105C5 63                    .data.b h'63 ; c
ROM:000105C6 63                    .data.b h'63 ; c
ROM:000105C7 62                    .data.b h'62 ; b
ROM:000105C8 61                    .data.b h'61 ; a
ROM:000105C9 60                    .data.b h'60 ; `
ROM:000105CA 58                    .data.b h'58 ; X
ROM:000105CB 44                    .data.b h'44 ; D
ROM:000105CC 42                    .data.b h'42 ; B
ROM:000105CD 42                    .data.b h'42 ; B
ROM:000105CE 42                    .data.b h'42 ; B
ROM:000105CF 42                    .data.b h'42 ; B
ROM:000105D0 42                    .data.b h'42 ; B
ROM:000105D1 42                    .data.b h'42 ; B
ROM:000105D2 42                    .data.b h'42 ; B
```
## References
* Chip Manual `chip_manual_sh_2esm.pdf` is included in the repo
* ROM File `unk.bin` is included in the repo
